import { movieTitleToSlug } from "./slug.js";

export function fetchMoviesAsync(connection) {
  return new Promise((resolve, reject) => {
    fetchMovies(connection, (error, results) => {
      if (error)
        reject(new QueryError("Error performing SQL query", { cause: error }));
      resolve(results);
    });
  });
}

function fetchMovies(connection, callback) {
  connection.query(
    `
        SELECT DISTINCT JSON_OBJECT(
                'titel', CONVERT(titel USING utf8),
                'zusatz', CONVERT(zusatz using utf8),
                'header', CONVERT(header using utf8),
                'beschreibung', CONVERT(beschreibung using utf8)
        ) AS json_result
        FROM _traumstern_film
        JOIN _traumstern_zeitpaar ON fk_film_id = _traumstern_film.id
        WHERE _traumstern_zeitpaar.start > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
        GROUP BY titel;
    `,
    (error, results) => {
      callback(
        error,
        results?.map((result) => JSON.parse(result.json_result)),
      );
    },
  );
}

export function augmentWithId(movie) {
  return {
    id: movieTitleToSlug(movie.titel),
    ...movie,
  };
}

export class QueryError extends Error {}
