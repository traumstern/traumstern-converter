import express from "express";
import dotenv from "dotenv";
import { getConnectionAsync, initPool } from "./mysql-connection.js";
import { fetchMoviesAsync, augmentWithId } from "./movies.js";
import { fetchDatesWithMoviesAsync } from "./dates-with-movies.js";
import { movieTitleToSlug } from "./slug.js";

dotenv.config();
const pool = initPool();
const app = express();
const port = process.env.PORT || 3000;

// GET route '/movies'
app.get("/movies", async (req, res) => {
  try {
    const connection = await getConnectionAsync(pool);
    const movies = await fetchMoviesAsync(connection);
    connection.release();
    res.json(movies.map((movie) => augmentWithId(movie)));
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.get("/dates", async (req, res) => {
  try {
    const connection = await getConnectionAsync(pool);
    const spielplaene = await fetchDatesWithMoviesAsync(connection);
    connection.release();
    res.json(
      spielplaene.map((spielplan) => ({
        ...spielplan,
        movies: spielplan.movies.map((movie) => ({
          startTime: movie.startTime,
          id: movieTitleToSlug(movie.titel),
        })),
      })),
    );
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
