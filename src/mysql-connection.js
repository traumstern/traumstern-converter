import mysql from "mysql";

export function initPool() {
  return mysql.createPool({
    connectionLimit: 10,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
  });
}

export async function getConnectionAsync(pool) {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        reject(new ConnectionError("Connection failed", { cause: err }));
      }
      resolve(connection);
    });
  });
}

export class ConnectionError extends Error {}
