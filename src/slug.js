import slugify from "slugify";

export function movieTitleToSlug(movieTitle) {
  return slugify(movieTitle.toLowerCase());
}
