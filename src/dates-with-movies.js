import { movieTitleToSlug } from "./slug.js";

export function fetchDatesWithMoviesAsync(connection) {
  return new Promise((resolve, reject) => {
    fetchDatesWithMovies(connection, (error, results) => {
      if (error)
        reject(new QueryError("Error performing SQL query", { cause: error }));
      resolve(results);
    });
  });
}

function fetchDatesWithMovies(connection, callback) {
  connection.query(
    `   SELECT DATE_FORMAT(start, '%Y-%m-%d') as date, JSON_ARRAYAGG(
            JSON_OBJECT(
                'titel', CONVERT(titel using utf8),
                'startTime', DATE_FORMAT(start, '%H:%i')
            )
        ) as movies
        FROM _traumstern_zeitpaar
        JOIN _traumstern_film ON _traumstern_film.id = fk_film_id
        WHERE _traumstern_zeitpaar.start > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
        GROUP BY date;
    `,
    (error, results) => {
      callback(
        error,
        results?.map((result) => ({
          date: result.date,
          movies: JSON.parse(result.movies),
        })),
      );
    },
  );
}

export class QueryError extends Error {}
